package dragdrop.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.DragDropEvent;

import dragdrop.pojo.Ingrediente;
import dragdrop.pojo.ProdutoMenu;
import dragdrop.service.IngredienteService;
import dragdrop.service.ProdutoService;

@ManagedBean(name="produtoController")
@SessionScoped
public class ProdutoMenuController implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5033084466300854938L;
	private ProdutoService produtoService = new ProdutoService();
	private IngredienteService ingredienteService = new IngredienteService();
	
	private List<ProdutoMenu> produtos;
	private List<Ingrediente> ingredientesDisponiveis;
	private List<Ingrediente> droppedIngredientes;
	private ProdutoMenu produtoSelecionado;
	private Ingrediente ingredienteSelecionado;
	
	public ProdutoMenuController() {
		produtos = produtoService.gerarProdutos();
		ingredientesDisponiveis = ingredienteService.GerarIngredientes();
		droppedIngredientes = new ArrayList<Ingrediente>();
	}
	
	public void onDropIngrediente(DragDropEvent ddEvent) {
        Ingrediente ingrediente = ((Ingrediente) ddEvent.getData());
        droppedIngredientes.add(ingrediente);
        ingredientesDisponiveis.remove(ingrediente);
    }
	
	public String incluirIngredientes(){
		ingredientesDisponiveis = ingredienteService.GerarIngredientes();
		droppedIngredientes = new ArrayList<Ingrediente>();
		return "incluiIngredientes.jsf";
	}
	
	public String gravarProduto() {
		this.produtoSelecionado.setIngredientes(droppedIngredientes);
		return "listaprodutos.jsf";
	}
	

	public List<Ingrediente> getIngredientesDisponiveis() {
		return ingredientesDisponiveis;
	}

	public void setIngredientesDisponiveis(List<Ingrediente> ingredientesDisponiveis) {
		this.ingredientesDisponiveis = ingredientesDisponiveis;
	}

	public List<Ingrediente> getDroppedIngredientes() {
		return droppedIngredientes;
	}

	public void setDroppedIngredientes(List<Ingrediente> droppedIngredientes) {
		this.droppedIngredientes = droppedIngredientes;
	}

	public Ingrediente getIngredienteSelecionado() {
		return ingredienteSelecionado;
	}

	public void setIngredienteSelecionado(Ingrediente ingredienteSelecionado) {
		this.ingredienteSelecionado = ingredienteSelecionado;
	}

	public List<ProdutoMenu> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoMenu> produtos) {
		this.produtos = produtos;
	}

	public ProdutoMenu getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(ProdutoMenu produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
	
}

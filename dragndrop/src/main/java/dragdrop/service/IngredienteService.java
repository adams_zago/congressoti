package dragdrop.service;

import java.util.ArrayList;
import java.util.List;

import dragdrop.pojo.Ingrediente;

public class IngredienteService {

	public List<Ingrediente> GerarIngredientes() {
		List<Ingrediente> ingredientesRetorno = new ArrayList<Ingrediente>();
		ingredientesRetorno.add(new Ingrediente(1L, "Fil� Mignon"));
		ingredientesRetorno.add(new Ingrediente(2L, "Tomate"));
		ingredientesRetorno.add(new Ingrediente(3L, "Queijo Parmes�o"));
		ingredientesRetorno.add(new Ingrediente(4L, "Cebola"));
		ingredientesRetorno.add(new Ingrediente(5L, "Cupim"));
		ingredientesRetorno.add(new Ingrediente(6L, "Frango Frito"));
		ingredientesRetorno.add(new Ingrediente(7L, "Manjeric�o"));
		ingredientesRetorno.add(new Ingrediente(8L, "Vinagrete"));
		ingredientesRetorno.add(new Ingrediente(9L, "Mandioca Frita"));
		ingredientesRetorno.add(new Ingrediente(0L, "Pimenta Calabresa"));
		
		return ingredientesRetorno;
	}

	
}

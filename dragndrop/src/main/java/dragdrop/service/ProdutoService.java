package dragdrop.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import dragdrop.pojo.ProdutoMenu;

public class ProdutoService {

	public List<ProdutoMenu> gerarProdutos() {
		List<ProdutoMenu> produtosRetorno = new ArrayList<ProdutoMenu>();
		
		produtosRetorno.add(new ProdutoMenu(1l, "Carne na Chapa", new BigDecimal(23.56D)));
		produtosRetorno.add(new ProdutoMenu(2l, "Frango a Passarinho", new BigDecimal(15.89D)));
		produtosRetorno.add(new ProdutoMenu(3l, "Cupim Casqueado", new BigDecimal(35.45D)));
		
		return produtosRetorno;
	}

	
}
